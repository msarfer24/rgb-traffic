const { getTrafficHexColor } = require('./traffic-light')
const colors = require('colors')

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function trafficLight() {
  console.log(getTrafficHexColor('GREEN').green)
  await sleep(3000)
  console.log(getTrafficHexColor('YELLOW').yellow)
  await sleep(1000)
  console.log(getTrafficHexColor('RED').red)
  
  const nonColor = getTrafficHexColor('BLUE')
  if (nonColor === undefined){
    console.log('BLUE not is a traffic light color')
  }else {
    console.log(getTrafficHexColor('BLUE').blue)
  }
}


colors.enable()
trafficLight()